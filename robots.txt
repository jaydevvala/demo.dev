#Googlebot
User-agent: Googlebot
Disallow: /wp-admin/
Allow: *.css
Allow: *.js

# Other bots
User-agent: *
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /wp-content/plugins/


User-agent: 008
Disallow: /
User-Agent: AhrefsBot
Disallow: /
User-agent: MJ12bot
Disallow: /
User-agent: Yandex
Disallow: /
User-agent: proximic
Disallow: /
User-agent: Baiduspider
Disallow: /
User-agent: Searcharoo
Disallow: /
User-agent: FatBot
Disallow: /
User-agent: TurnitinBot
Disallow: /

Sitemap: http://template.dev/sitemap.xml