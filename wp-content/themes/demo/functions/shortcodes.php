<?php

// SITE URL for links
function home_shortcode() {
  return SITE;
}
add_shortcode( 'home', 'home_shortcode' );

// Wordpress URL for links
function image_shortcode() {
  return WP;
}
add_shortcode('image', 'image_shortcode');

// Wordpress URL for Uploads
function my_uploads_folder () {
  $upload_dir = wp_upload_dir();
  return $upload_dir['baseurl'];
}
add_shortcode('uploads', 'my_uploads_folder');

function button_shortcode($atts) {

  $atts = shortcode_atts( array(
    'link' => '#',
    'title' => 'Visit this link',
    'label' => 'Click here'
  ), $atts );
  extract($atts);
  $output = '';
  $output .= '<a class="o-btn o-btn--small" href="' . $link . '" title="' . $title . '">' . $label . '</a>';

  return $output;
}
add_shortcode('button', 'button_shortcode');

?>