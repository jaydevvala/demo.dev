<?php

// Define some global variables site site url, css, js and other assets
define( 'URL', home_url() );
define( 'SITE', home_url() );
define( 'WP', site_url() );
define( 'STYLESHEET_URL', esc_url( get_stylesheet_directory_uri() ) );
define( 'CSS', STYLESHEET_URL.'/assets/css');
define( 'JS', STYLESHEET_URL.'/assets/js');
define( 'LIB', STYLESHEET_URL.'/assets/lib');
define( 'ASSETS', STYLESHEET_URL.'/assets');

$GLOBALS['site_cachebust'] = 1442824767564;

// Get stylesheet url
function get_site_css() {
  $site_css = (is_localhost()) ? CSS.'/screen.dev.css' : CSS.'/screen.min.css';
  return $site_css.'?v='.$GLOBALS['site_cachebust'];
}

// Website created by credit link
function credit_link() {
  ?><a href="http://rockagency.com.au/" title="Visit this external site">Rock&nbsp;Agency</a><?php
}

// Wordpress admin link
function login_link() {
  ?>
  <a title="Administer this website" href="<?php echo WP; ?>/wp-admin/" rel="nofollow">Login</a>
  <?php
}

function pagedNav( $extraClasses=array(), $forward_text= 'Newer posts &rarr;', $backward_text= '&larr; Older posts' ) {
  global $wp_query;
  if (  $wp_query->max_num_pages > 1 ) :
    $navClasses = array_merge(array('c-pagenav  o-layout'),$extraClasses);
    $navClasses = implode(' ',$navClasses);
  ?>
    <nav class="<?php echo $navClasses; ?>">
      <span class="c-pagenav__item  c-pagenav__item--previous  o-layout__item  u-one-half"><?php next_posts_link( $backward_text ); ?></span><?php
      ?><span class="c-pagenav__item  c-pagenav__item--next  o-layout__item  u-one-half"><?php previous_posts_link( $forward_text ); ?></span>
    </nav>
  <?php endif;
}

function site_excerpt_length( $length ) {
  return 100;
}
add_filter( 'excerpt_length', 'site_excerpt_length' );

function excerpt($id='', $limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = '<p>' . implode(" ", $excerpt).' ...&nbsp;<a href="'. get_permalink($id) .'" title="Read more &rarr;">read&nbsp;more</a></p>';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}

// Load svgicon from sprite
function svgicon($id='', $viewbox='0 0 32 32', $extraclasses='', $fallback='') {
  //Note that iOS Safari 6.0 doesn't like self-closing <use/>, so add separate closing tag to keep it valid XML.
  // xmlns necessary to give layout in IE8 (although we have dropped support for IE8 for H&A)
  ?><svg viewBox="<?php echo $viewbox; ?>" xmlns="http://www.w3.org/2000/svg" version="1.1" class="c-svgicon c-svgicon--<?php echo $id; ?> <?php echo $extraclasses; ?>"><use xlink:href="<?php echo ASSETS; ?>/img/inline.svg#<?php echo $id; ?>"></use></svg><?php
  if (!empty($fallback)) {
    ?><span class="c-svgicon-fallback"><?php echo $fallback; ?></span><?php
  }
}

function get_svgicon($id='', $viewbox='0 0 32 32', $extraclasses='', $fallback='') {
  ob_start();
  svgicon($id, $viewbox, $extraclasses, $fallback);
  $output = ob_get_contents();
  ob_end_clean();
  return $output;
}

// svg icon shortcode
function svgicon_shortcode ($atts) {
  extract( shortcode_atts( array(
    'id'            => '',
    'extraclasses'  => '',
    'fallback'      => '',
    'viewbox'     => '0 0 32 32'
  ), $atts ) );
  $output = get_svgicon($id, $viewbox, $extraclasses, $fallback);
  return $output;
}
add_shortcode('svgicon', 'svgicon_shortcode');

// Add nav class depending on condition check for nav items when required
function special_nav_class($classes, $item){
  // if( ( is_single() || is_archive() || is_category() ) && $item->title == "News"){
  //   $classes[] = "current-menu-item";
  // }
  return $classes;
}
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

?>