<?php

// Set gallery image thumbnail size
add_image_size( 'gallery', 368, 264, true );

// Removes default gallery styling
add_theme_support( 'cleaner-gallery' );

?>