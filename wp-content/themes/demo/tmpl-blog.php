<?php
/*
 * Template Name: Blog Page
 */
?>

<?php get_header(); ?>
<main class="c-main-content o-main" role="main">
  <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
    <article <?php post_class(); ?>>
      <h1 class="u-alpha"><?php the_title(); ?></h1>
      <div class="c-cms-content">
        <?php the_content(); ?>
      </div>
      <?php

        $items = new WP_Query();
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $items->query(array(
          'post_type' => 'post',
          'status' => 'published',
          'posts_per_page' => 6,
          'paged' => $paged
        ));

        $orig_query = $wp_query;
        $wp_query = $items;
        get_template_part( 'loop', 'row' );
        pagedNav();
        $wp_query = $orig_query;
      ?>

      <?php wp_link_pages(); ?>
    </article>
  <?php endwhile; ?>
</main>

<?php

get_sidebar();

?>
<?php get_footer(); ?>