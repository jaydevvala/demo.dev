<?php
  get_header();
?>
<div class="c-content">
  <div class="c-wrapper">
    <main id="Main" class="c-main-content o-main">      
      <?php if ( have_posts() ) while ( have_posts() ) : the_post() ; ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <h1 class="post u-alpha"><?php the_title(); ?></h1>
        <div class="c-cms-content">
          <?php the_content(); ?>
        </div>
          <?php wp_link_pages(); ?>
      </article>
      <?php endwhile; ?>

  <?php if ( have_rows('featured_content') ) : ?>
    <div class="c-content__learn-more">
      <?php while ( have_rows('featured_content') ) : the_row();
        $button_text = 'Learn more';
        if ( get_sub_field('button_text') ) {
          $button_text = get_sub_field('button_text');
        }
        $link = get_sub_field('link');
        $title = get_sub_field('title');
        $icon = get_sub_field('icon');
        $desc = get_sub_field('description');
      ?><article>
          <div class="c-content__leanr-more--icon"><a href="<?= $link; ?>"><?php svgicon($icon, '0 0 65 65', 'c-module__icon'); ?></a></div>
          <div class="c-content__learn-more--text">
            <a href="<?= $link; ?>"><h2><?= $title ?></h2></a>
            <p><?= $desc ?></p>
            <a href="<?= $link; ?>" class="btn"><?= $button_text ?></a>
          </div>


      <!--       <div class="c-module__thumb">
              <a class="c-module__wraplink" href="<?= $link; ?>" rel="bookmark">
                <?php svgicon($icon, '0 0 65 65', 'c-module__icon'); ?>
              </a>
            </div>
            <div class="c-module__text">
              <a class="c-module__wraplink" href="<?= $link ?>" rel="bookmark">
                <h2 class="c-module__heading u-beta"><?= $title ?></h2>
              </a>
              <p><?= $desc ?></p>
            </div>
            <div class="c-meta--featured">
              <a href="<?= $link ?>" class="o-btn o-btn--small" onclick="ga('send', 'event', 'Buttons', 'click', '<?=$title?>');"><?= $button_text ?></a>
            </div>
          </div> -->
        </article><?php endwhile; ?>
        <article class="learn-more__contact">
          <div class="c-content__learn-more--text">
            <a href="#"><h2>We are <span>open</span> every weekday and <span>saturdays</span>. </h2></a>
            <p>Level 1, 330 Glen Eira Road, <br> Elsternwick, 3185</p>
            <p class="phone">
              <img src="<?php echo ASSETS; ?>/img/phone.svg" alt="<?php echo AUTHOR; ?>"> 
              <a href="#">03 9530 0044</a>
            </p>
            <a href="#" class="btn">Contact Us</a>
          </div>
        </article>
    </div>
  <?php endif; ?>





    </main>
  </div>
</div>
<div class="c-bottom">
  <div class="map"><h1>this is where the map go</h1></div>
  <div class="quick-links">
    <section>
      <div class="quick-links--latest-news">
        <ul>
          <h2>Latest News</h2>
          <li><a href="#">Cranial Osteo - what's all that about?</a></li>
          <li><a href="#">Headaches, how can we treat them?</a></li>
          <li><a href="#">Taping into Pain</a></li>
          <li><a href="#">Preseason Optimism</a></li>
          <li><a href="#">Don’t Worry, Be Happy!</a></li>
          <li><a href="#">Double Daylight Saving!</a></li>
        </ul>
      </div>
    </section>
    <section>
      <div class="quick-links--quick-contact">
        <h2>Quick Contact</h2>
        <p>Phone: 03 9530 0044 <br> Fax: 03 9018 4463 <br> Email: michael@elsternwickosteopathy.com.au <br> Address: Level 1, 330 Glen Eira Road, Elsternwick, 3185</p>
        <ul class="quick-links__social-media">
          <li><a href="#"><img src="<?php echo ASSETS; ?>/img/fb.svg" alt="<?php echo AUTHOR; ?>"></a></li>
          <li><a href="#"><img src="<?php echo ASSETS; ?>/img/inst.svg" alt="<?php echo AUTHOR; ?>"></a></li>
          <li><a href="#"><img src="<?php echo ASSETS; ?>/img/tweeter.svg" alt="<?php echo AUTHOR; ?>"></a></li>
          <li><a href="#"><img src="<?php echo ASSETS; ?>/img/li.svg" alt="<?php echo AUTHOR; ?>"></a></li>
          <li><a href="#"><img src="<?php echo ASSETS; ?>/img/gplus.svg" alt="<?php echo AUTHOR; ?>"></a></li>
        </ul>
      </div>
    </section>
    <section>
      <div class="quick-links--logo">
        <p>Call us today on<br><a href="#">03 9530 0044</a></p>
      </div>
    </section>
  </div>
</div>
<?php
//  get_sidebar();
  get_footer();
?>


