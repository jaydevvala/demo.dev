<?php
  get_header();
?>
<div class="c-content">
  <div class="c-wrapper">
    <main id="Main" class="c-main-content o-main">
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <h1 class="post u-alpha">Elsternwick Osteopathy, Melbourne</h1>
        <div class="c-cms-content">
          <p>Elsternwick Osteopathy is determined to provide safe and effective Osteo care to our patients in Melbourne. As Osteopaths, we understand our patients’ injuries and know how to manage their pain. We love our job and we treat our patients with respect and maintain a high level of professionalism.  We look forward to treating you.</p>
        </div>
      </article>
      <div class="c-content__learn-more">
        <article>
          <div class="c-content__leanr-more--icon"><a href="#"><img src="<?php echo ASSETS; ?>/img/o-p.svg" alt="<?php echo AUTHOR; ?>"></a></div>
          <div class="c-content__learn-more--text">
            <a href="#"><h2>Our practitioners</h2></a>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lobortis erat tortor. </p>
            <a href="#" class="btn">Learn More</a>
          </div>
        </article>
        <article>
          <div class="c-content__leanr-more--icon"><a href="#"><img src="<?php echo ASSETS; ?>/img/what.svg" alt="<?php echo AUTHOR; ?>"></a></div>
          <div class="c-content__learn-more--text">
            <a href="#"><h2>What is Osteo?</h2></a>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lobortis erat tortor. </p>
            <a href="#" class="btn">Learn More</a>
          </div>
        </article>
        <article>
          <div class="c-content__leanr-more--icon"><a href="#"><img src="<?php echo ASSETS; ?>/img/we-treat.svg" alt="<?php echo AUTHOR; ?>"></a></div>
          <div class="c-content__learn-more--text">
            <a href="#"><h2>What we treat</h2></a>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc lobortis erat tortor. </p>
            <a href="#" class="btn">Learn More</a>
          </div>
        </article>
        <article class="learn-more__contact">
          <div class="c-content__learn-more--text">
            <a href="#"><h2>We are <span>open</span> every weekday and <span>saturdays</span>. </h2></a>
            <p>Level 1, 330 Glen Eira Road, <br> Elsternwick, 3185</p>
            <p class="phone">
              <img src="<?php echo ASSETS; ?>/img/phone.svg" alt="<?php echo AUTHOR; ?>"> 
              <a href="#">03 9530 0044</a>
            </p>
            <a href="#" class="btn">Contact Us</a>
          </div>
        </article>
      </div>
    </main>
  </div>
</div>
<div class="c-bottom">
  <div class="map"><h1>this is where the map go</h1></div>
  <div class="quick-links">
    <section>
      <div class="quick-links--latest-news">
        <ul>
          <h2>Latest News</h2>
          <li><a href="#">Cranial Osteo - what's all that about?</a></li>
          <li><a href="#">Headaches, how can we treat them?</a></li>
          <li><a href="#">Taping into Pain</a></li>
          <li><a href="#">Preseason Optimism</a></li>
          <li><a href="#">Don’t Worry, Be Happy!</a></li>
          <li><a href="#">Double Daylight Saving!</a></li>
        </ul>
      </div>
    </section>
    <section>
      <div class="quick-links--quick-contact">
        <h2>Quick Contact</h2>
        <p>Phone: 03 9530 0044 <br> Fax: 03 9018 4463 <br> Email: michael@elsternwickosteopathy.com.au <br> Address: Level 1, 330 Glen Eira Road, Elsternwick, 3185</p>
        <ul class="quick-links__social-media">
          <li><a href="#"><img src="<?php echo ASSETS; ?>/img/fb.svg" alt="<?php echo AUTHOR; ?>"></a></li>
          <li><a href="#"><img src="<?php echo ASSETS; ?>/img/inst.svg" alt="<?php echo AUTHOR; ?>"></a></li>
          <li><a href="#"><img src="<?php echo ASSETS; ?>/img/tweeter.svg" alt="<?php echo AUTHOR; ?>"></a></li>
          <li><a href="#"><img src="<?php echo ASSETS; ?>/img/li.svg" alt="<?php echo AUTHOR; ?>"></a></li>
          <li><a href="#"><img src="<?php echo ASSETS; ?>/img/gplus.svg" alt="<?php echo AUTHOR; ?>"></a></li>
        </ul>
      </div>
    </section>
    <section>
      <div class="quick-links--logo">
        <p>Call us today on<br><a href="#">03 9530 0044</a></p>
      </div>
    </section>
  </div>
</div>
<?php
//  get_sidebar();
  get_footer();
?>


