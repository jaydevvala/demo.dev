<footer class="c-cms-meta">
  <?php if( is_single() ) { ?>
    Posted on: <?php echo get_the_date(); echo "&nbsp;&nbsp;"; } ?>
  <?php if( get_the_term_list( $post->ID, 'category', '', ', ', '' ) ){ ?>
    Category: <?php echo get_the_term_list( $post->ID, 'category', '', ', ', '' ); } ?>
  <?php if( get_the_term_list( $post->ID, 'post_tag', '', ', ', '' ) ){ ?>
    &nbsp;&nbsp; Tags: <?php echo get_the_term_list( $post->ID, 'post_tag', '', ', ', '' ); ?>
<?php } ?>
</footer>