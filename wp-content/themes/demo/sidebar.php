<aside id="secondary" class="c-side-content o-side" role="complementary">
  <?php
    if ( is_active_sidebar( 'aside-sidebar' ) ) :
      dynamic_sidebar( 'aside-sidebar' );
    endif;
  ?>
</aside>
