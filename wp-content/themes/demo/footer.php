</div><!-- .c-content .o-wrapper-->
<footer>
	<div class="c-wrapper footer-wrapper">
		<div class="footer-links">
			<ul>
				<li>© Copyright Elsternwick Osteopathy 2015</li>
				<li><a href="#">Terms & Conditions</a></li>
				<li><a href="#">Privacy</a></li>
				<li>Website by <a href="#">Rock Agency</a>.</li>
			</ul>
		</div>
		<div class="footer-logo">
			<a href="#"><img src="<?php echo ASSETS; ?>/img/mem-logo.svg" alt="<?php echo AUTHOR; ?>"></a>
		</div>
	</div>
</footer>
  <?php wp_footer(); ?>
</body>
</html>