<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?> dir="ltr">
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="x-ua-compatible" content="ie=edge"> <!-- make IE render page content in highest IE mode available -->
  <title><?php wp_title(); ?> </title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="author" content="<?php echo AUTHOR; ?>">
  <meta name="siteurl" content="<?php echo URL; ?>" id="Siteurl">
  <meta name="themeurl" content="<?php echo STYLESHEET_URL; ?>" id="Themeurl">
  <link rel="icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/favicon.ico">
  <link rel="apple-touch-icon" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-iphone-retina.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo STYLESHEET_URL; ?>/assets/img/touch-icon-ipad-retina.png">
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/style.css" media="screen" />
  <?php include('partials/inlinescripts.min.php'); ?>
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="c-top">
  <div class="c-top__inner">
    <header role="banner" class="c-banner o-wrapper">
      <?php echo $logo_wrap_open; ?>
        <img src="<?php echo ASSETS; ?>/img/logo.svg" alt="<?php echo AUTHOR; ?>">
      <?php echo $logo_wrap_close; ?>
      <nav class="c-site-nav" role="navigation">
        <?php
          wp_nav_menu( array(
            'theme_location' => 'primary-menu',
            'container' => false,
            'menu_class' => 'c-nav',
            'menu_id' => 'menu'
          ));
        ?>
      </nav>
    </header>
  </div>
</div>
<div class="c-stage"> 
  <div class="c-stage--wrapper">
    <div class="c-stage__content">
      <div class="c-stage__phone">
        <p>Call us today on<br><a href="#">03 9530 0044</a></p>
      </div>
      <div class="c-stage__moto">
        <h1>Safe and effective Osetopathic treatment.</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque quis luctus orci. Sed elementum, lectus sit amet.</p>
        <a href="#" class="btn c-stage__btn">Book An Appointment Online</a>
      </div>
    </div>
  </div>
</div>

<div class="c-content o-wrapper">
