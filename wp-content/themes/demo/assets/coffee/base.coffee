( ($) ->
  window.console or= {log: -> }
  window.site or= {}

  $.extend site,
    init_vars: ->
      @body = $('body')
      @root = $('html')
      @header = $('#Top')
      @main = $('#Main')
      @side = $('#Secondary')
      @siteurl = $('#Siteurl').attr('content')
      @themeurl = $('#Themeurl').attr('content')

    onready: ->
      @init_vars()
      @gallery()

    onload: ->

    gallery: ->
      gallery = $('.gallery')
      if gallery.length > 0
        loadCSS("#{@themeurl}/assets/css/jquery.fancybox.css")
        $.getScript "#{@themeurl}/assets/lib/jquery.fancybox.js", ->
          gallery.addClass('o-layout').addClass('o-layout--small')
          $('.gallery-item').addClass('o-layout__item u-one-half u-wide-palm-one-third u-wall-one-quarter')
          $('.fancybox').fancybox({
            beforeShow: ->
              alt = this.element.find('img').attr('alt')
              this.inner.find('img').attr('alt', alt)
              this.title = alt
          })

  $ ->
    site.onready()

  $(window).load ->
    site.onload()

)(jQuery)